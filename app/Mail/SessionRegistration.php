<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SessionRegistration extends Mailable {

    use Queueable, SerializesModels;

    public $subject = 'Session Registration On ';

    public function __construct($data) {
        $this->data = $data;
        $this->subject .= formatDate($this->data['schedule_date']);
    }

    public function build() {
        return $this->markdown('emails.appointment.session-registration')->subject($this->subject)->with(['data' => $this->data]);
    }
}
