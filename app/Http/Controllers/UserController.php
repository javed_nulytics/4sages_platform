<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\ImageManager;

use PDF;

class UserController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $userId = Auth::user()->id;
        $users = "";
        $ranks = "";

        if (Auth::user()->role == "Admin" || Auth::user()->role == "admin") {
            $users = DB::table('users')
                ->select('id', 'name', 'email', 'role', 'active', 'temporary_id', 'created_at')
                ->get();

            $ranks = DB::table('student_ranking')
                ->leftJoin('users', 'users.id', '=', 'student_ranking.student_id')
                ->select('users.id', 'users.name', 'student_ranking.percentage', 'student_ranking.percentile')
                ->orderBy('student_ranking.percentage', 'desc')
                ->get();
        }
        /*
                SELECT `tutorial_completed_id`, `student_id`, `syllabus_id`, `completed_percentage`, `c_time`
        FROM tutorial_completed AS t1
        WHERE
        (SELECT COUNT(*)
                    FROM tutorial_completed AS t2
                    WHERE t2.syllabus_id = t1.syllabus_id AND t2.completed_percentage > t1.completed_percentage
            ) = 0
        */


        $tutorial_completed =  DB::select('SELECT * FROM tutorial_completed AS t1 WHERE (SELECT COUNT(*) FROM tutorial_completed AS t2 WHERE t2.syllabus_id = t1.syllabus_id AND t2.completed_percentage > t1.completed_percentage) = 0 AND t1.student_id=' . $userId);


        $user_data = DB::table('users')
            ->where('id', '=', $userId)
            ->first();

        $course_data = DB::table('courses')
            ->where('tutor_id', '=', $userId)
            ->get();

        $quiz_data = DB::table('quiz_score')
            ->leftJoin('syllabus', 'syllabus.id', '=', 'quiz_score.syllabus_id')
            ->select('quiz_score.score', 'quiz_score.percentile', 'quiz_score.created_at', 'quiz_score.syllabus_id', 'syllabus.unit_title')
            ->where('student_id', '=', $userId)
            ->get();

        $myObj = array();
        $analytics = array();

        $student_id = $userId;

        $syllabus = DB::table('syllabus')
            ->select('syllabus.id', 'syllabus.unit_title', 'courses.course_id', 'courses.course_name')
            ->rightJoin('courses', 'courses.course_id', '=', 'syllabus.course_id')
            ->where('syllabus.id', '>', 0)
            ->get();



        foreach ($syllabus as $syllabus) {
            $quiz_view_id = DB::table('quiz_score')
                ->select('quiz_id', 'score')
                ->distinct('quiz_id')
                ->orderBy('score', 'desc')
                ->where('syllabus_id', '=', $syllabus->id)
                ->where('student_id', '=', $student_id)
                ->limit(1)
                ->get();

            foreach ($quiz_view_id as $quiz) {
                $myObj[0] = $syllabus->unit_title;
                $myObj[1] = $quiz->score;
                array_push($analytics, $myObj);
            }
        }

        return view('4sages.profile.index', [
            'user_data' => $user_data,
            'course_data' => $course_data,
            'quiz_data' => $quiz_data,
            'users' => $users,
            'ranks' => $ranks,
            'analytics' => $analytics,
            'tutorial_completed' => $tutorial_completed,
        ]);
    }


    public function edit(Request $request){

        $data = $request->input('params');
        $userId = Auth::user()->id;
        $date = date('Y-m-d H:i:s');

        DB::table('users')
            ->where('id', $userId)
            ->update([
                'name' => $data['name'],
                'first_name' => $data['first_name'],
                'last_name'=> $data['last_name'],
                'bio'=> $data['bio'],
                'updated_at' => $date,
            ]);

        return json_encode("success");
    }


    public function upload_profile_picture(Request $request) {

        $userId = Auth::user()->id;
        create_directory();
        $file = Input::file('image');

        $rules = array('file' => 'required');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {

            $post_mime_type = $file->getMimeType();

            if (strpos($post_mime_type, 'image') === 0) {

                $data = getimagesize($file);
                $width = $data[0];
                $height = $data[1];


                $destinationPath = 'profile_photo';
                $fileName = substr(rand(), 0, 5) . '_' . preg_replace('/\s+/', '_', $file->getClientOriginalName());
                Input::file('image')->move($destinationPath, $fileName);

                $this->createThumbnail($fileName);

                DB::table('users')
                    ->where('id', $userId)
                    ->update([
                        'image_title' => $fileName
                    ]);

            }
        }
        return redirect("/profile");
    }


    function createThumbnail($postTitle) {

        // configure with favored image driver (gd by default)
        Image::configure(array('driver' => 'gd'));

        // creating thumbnail image
        $original_image = "profile_photo/" . $postTitle;
        $thumbnail_image = "profile_photo/thumbnail/" . $postTitle;
        $preview_image = "profile_photo/preview/" . $postTitle;
        $small_image = "profile_photo/small/" . $postTitle;

        $image = Image::make($original_image)->resize(86, 92)->save($thumbnail_image);
        $image = Image::make($original_image)->resize(48, 48)->save($small_image);
        //$image = Image::make($original_image)->resize(468, 543)->save($preview_image);

        $image = Image::make($original_image)->resize(null, 720, function ($constraint) {
            $constraint->aspectRatio();
        })->save($preview_image);



//        $image = Image::make($original_image)->resize(null, 450, function ($constraint) {
//            $constraint->aspectRatio();
//        })->save($preview_image);

    }


    public function editUserStatus(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('users')
            ->where('id', $data['user_id'])
            ->update([
                $data['db_col'] => $data['value'],
                'updated_at' => $date,
            ]);

        echo json_encode("success");

    }


    public function downloadPDF() {

//        $user = DB::table('users')->where('id', 1)->first();

        $userId = Auth::user()->id;
        $users = "";
        $ranks = "";

        if (Auth::user()->role == "Admin" || Auth::user()->role == "admin") {

            $ranks = DB::table('student_ranking')
                ->leftJoin('users', 'users.id', '=', 'student_ranking.student_id')
                ->select('users.id', 'users.name', 'student_ranking.percentage', 'student_ranking.percentile')
                ->orderBy('student_ranking.percentage', 'desc')
                ->get();
        }

        $pdf = PDF::loadView('4sages.profile.pdf-report', compact('ranks'));
        return $pdf->download('4sages-Report.pdf');


//        return view('4sages.profile.pdf-report', [
//            'ranks' => $ranks
//        ]);



    }
}
