<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class EventController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $events = [];

        $events[] = \Calendar::event(
            'Arif Bhai Leaving BD', //event title
            true, //full day event?
            '2017-08-14T0800', //start time (you can also use Carbon instead of DateTime)
            '2017-08-14T0800', //end time (you can also use Carbon instead of DateTime)
            0 //optionally, you can specify an event ID
        );

        $events[] = \Calendar::event(
            "4Sages Platform Launching", //event title
            true, //full day event?
            new \DateTime('2017-08-17'), //start time (you can also use Carbon instead of DateTime)
            new \DateTime('2017-08-14'), //end time (you can also use Carbon instead of DateTime)
            'stringEventId' //optionally, you can specify an event ID
        );


        $calendar = \Calendar::addEvents($events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 6
        ]);

        return view('4sages.calendar.index', [
            'calendar' => $calendar
        ]);

    }

}
