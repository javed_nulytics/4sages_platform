<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Model\Schedule;
use App\Model\Appointment;
use Illuminate\Support\Facades\App;

class ScheduleController extends Controller {


    public function index() {

        $schedules = Schedule::orderBy('schedule_id', 'desc')->paginate(20);


        return view('4sages.schedule.index', [
            'schedules' => $schedules
        ]);

    }


    public function viewSchedule($schedule_id) {

        $schedule = Schedule::find($schedule_id);
        $appointments = Appointment::where('schedule_id', $schedule_id)->get();

        return view('4sages.schedule.schedule', [
            'schedule' => $schedule,
            'appointments' => $appointments,
        ]);

    }



    public function addSchedule(Request $request) {

        $created_by = Auth::user()->id;

        $request->validate([
            'schedule_date' => 'required|date',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        $schedule_date = date("Y-m-d", strtotime($request->input('schedule_date')));

        $schedule = new Schedule();

        $schedule->schedule_date = $schedule_date;
        $schedule->start_time = $request->input('start_time');
        $schedule->end_time = $request->input('end_time');
        $schedule->created_by = $created_by;

        $schedule->save();

        return redirect()->back();
    }

}
