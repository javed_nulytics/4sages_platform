<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Mail;

use App\Mail\SessionRegistration;

use App\Model\Appointment;
use App\Model\Schedule;

class AppointmentController extends Controller {



    public function index() {

        $user_id = Auth::user()->id;

        $schedules = DB::table('schedules')
            ->select('schedules.*',
                DB::raw("(SELECT COUNT(appointment_id) FROM appointments WHERE appointments.user_id=$user_id AND appointments.schedule_id=schedules.schedule_id) as user_count"))
            ->whereDate('schedules.schedule_date', '>=', date('Y-m-d'))
            ->orderBy('schedules.schedule_id', 'desc')
            ->paginate(20);

        return view('4sages.appointment.index', [
            'schedules' => $schedules
        ]);

    }


    public function createAppointment(Request $request) {

        $user_id = Auth::user()->id;

        $name = Auth::user()->name;
        $email = Auth::user()->email;

        $data = $request->input('params');

        $response = 'error';

        $schedule = Schedule::find($data['schedule_id']);

        if($schedule->no_of_guest < $schedule->max_guest) {

            $appointment = Appointment::firstOrNew(array('user_id' => $user_id, 'schedule_id'=> $data['schedule_id']));

            $appointment->save();
            $appointment_id = $appointment->appointment_id;

            if ($appointment->wasRecentlyCreated) {
                $response = 'success';

                $schedule->no_of_guest = ($schedule->no_of_guest + 1);
                $schedule->save();

                $data = [
                    'name' => $name,
                    'schedule_date' => $schedule->schedule_date,
                    'start_time' => $schedule->start_time,
                    'end_time' => $schedule->end_time,
                ];

                //Mail::to($email)->send(new SessionRegistration($data));

            }
        }

        return json_encode($response);
    }

}
