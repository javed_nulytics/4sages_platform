<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model {

    protected $table = 'schedules';
    protected $primaryKey = 'schedule_id';

    protected $guarded = [];


    public function creator() {
        return $this->belongsTo('App\User', 'id','created_by');
    }

}
