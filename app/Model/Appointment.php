<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model {

    protected $table = 'appointments';
    protected $primaryKey = 'appointment_id';

    protected $guarded = [];


    public function user() {
        return $this->belongsTo('App\User', 'user_id','id');
    }

    public function schedule() {
        return $this->belongsTo('App\Model\Schedule', 'schedule_id','schedule_id');
    }

}
