@extends('layouts.header')
@section('title', 'Appointment')

@section('content')

    <script src="/js/appointment.js"></script>


    <div class="container container-height">
        <div class="lms_course_syllabus lms_offer_courses">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 top-margin-50">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered table-striped" id="products-datatable">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($schedules as $schedule)
                                        <tr>
                                            <td>{{ formatShortDate($schedule->schedule_date) }}</td>
                                            <td>{{ date('g:i a', strtotime( $schedule->start_time )) }}</td>
                                            <td>{{ date('g:i a', strtotime( $schedule->end_time )) }}</td>
                                            {{--<td>{{ $schedule->no_of_guest<$schedule->max_guest ? 'Available':'Session full' }}</td>--}}
                                            <td>
                                                @if($schedule->user_count>0)
                                                    <span>You are registered to this session</span>
                                                @elseif($schedule->no_of_guest<$schedule->max_guest)
                                                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="makeAppointment({{ $schedule->schedule_id }})">
                                                        Schedule session
                                                    </button>
                                                @else
                                                    <span>Session full</span>
                                                @endif
                                            </td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection