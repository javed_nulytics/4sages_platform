@extends('layouts.header')
@section('title', 'Quiz')

@section('content')
    <script src="/js/quiz.js"></script>
    <link rel="stylesheet" href="/css/radio-button.css"/>
    <link href="/css/magic-check.css" rel="stylesheet">

    <div class="container container-height">
        <div class="row">
            <div class="col-lg-12" id="myform">

                <input type="hidden" id="quiz_view_id" value="{{$quiz_view_id}}">
                <input type="hidden" id="syllabus_id" value="{{$syllabus_id->course_topic_id}}">
                <input type="hidden" id="token" value="{{csrf_token()}}">


                @php $question_count = 0 @endphp

                @foreach($quiz_data as $quiz)

                    <script type="application/javascript">
                        <?php echo 'store_ans("'.$quiz->answer.'");'; ?>
                    </script>

                    <div class="col-md-12 top-margin-50">
                        <h4 style ="font-size: 30px">{{++$question_count}}. {{$quiz->question}}</h4>

                        <div class="col-md-4">
                            @if($quiz->media != "")
                                <img src="/questionimage/{{$quiz->media}}" width="40%" style="border-radius: 5px">
                            @endif
                        </div>
                        <div class="row">
                            <div style ="font-size: 17px" class="col-md-8">

                                <div class="">
                                    <input type="radio" class='magic-radio' name="option[{{$quiz->quiz_id}}]" id="option_id_{{$quiz->quiz_id}}_1" value="{{$quiz->option_1}}"/>
                                    <label for="option_id_{{$quiz->quiz_id}}_1">{{$quiz->option_1}}
                                        @if($quiz->option_1 == $quiz->answer)
                                            <i class="fa fa-check i_correct"></i>
                                        @endif
                                    </label>
                                </div>

                                <div class="">
                                    <input type="radio" class='magic-radio' name="option[{{$quiz->quiz_id}}]" id="option_id_{{$quiz->quiz_id}}_2" value="{{$quiz->option_2}}"/>
                                    <label for="option_id_{{$quiz->quiz_id}}_2">{{$quiz->option_2}}
                                        @if($quiz->option_2 == $quiz->answer)
                                            <i class="fa fa-check i_correct"></i>
                                        @endif
                                    </label>
                                </div>

                                <div class="">
                                    <input type="radio" class='magic-radio' name="option[{{$quiz->quiz_id}}]" id="option_id_{{$quiz->quiz_id}}_3" value="{{$quiz->option_3}}"/>
                                    <label for="option_id_{{$quiz->quiz_id}}_3">{{$quiz->option_3}}
                                        @if($quiz->option_3 == $quiz->answer)
                                            <i class="fa fa-check i_correct"></i>
                                        @endif
                                    </label>
                                </div>

                            </div>
                        </div>

                        @if($quiz->explanation != NULL || $quiz->explanation_media != Null)
                            <div class="row" id="explanation_div_{{$question_count}}" style="display: none">
                                <div class="col-md-8 top-margin-30">
                                    <h3 style ="font-size: 30px">Explanation</h3>
                                    <h4 style ="font-size: 22px" id ="answer_div_{{$question_count}}"></h4>
                                    @if($quiz->explanation != "" || $quiz->explanation != NULL)
                                        <p style ="font-size: 22px">{{$quiz->explanation}}</p>
                                    @endif
                                    @if($quiz->explanation_media != "" || $quiz->explanation_media != Null)
                                        <div class="col-md-4" style="padding-left: 0px">
                                            <img src="/explanation/{{$quiz->explanation_media}}" width="100%" style="border-radius: 5px">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif



                    </div>





                @endforeach


                <div class="col-md-12 top-margin-50">
                    <button type="submit" class="btn btn-lg btn-success" id="submit_quiz_button" value="Submit Quiz">Submit</button>
                </div>

                <div class="col-md-12">
                    <a type="button" class="btn btn-lg btn-success" id="back_button" href="/course/tutorial/{{ $syllabus_id->syllabus_id }}" style="display: none">Back To Topic</a>
                </div>

                <div class="col-md-12">
                    <button type="button" class="btn btn-lg btn-success" id="another_quiz"  style="display: none">Wanna take another quiz?</button>
                </div>

                <div class="col-md-12">
                    <button type="button" class="btn btn-lg btn-success" id="view_results"  style="display: none">View Result Details</button>
                </div>


                <div class="col-md-12" style="display: none" id="blog_list_div">
                    @if(count($blog_data) != 0)
                        <h2>Related Blogs</h2>
                        <ul class="lms_underlist_doted">

                            @foreach($blog_data as $blog)
                                <li><a style="font-size: 115%" href="{{ $blog->blog_link }}"> {{ $blog->blog_name == "" ? "Blog" :  $blog->blog_name }}</a></li>
                            @endforeach

                        </ul>
                    @endif
                </div>

                {!! Form::open(['url' => '/course/quiz/', 'id' => 'quiz_form']) !!}
                <input id="course_quiz" name="course_quiz" type="hidden" >
                {!! Form::close() !!}


                {!! Form::open(['url' => '/view/result/', 'id' => 'quiz_result_form']) !!}
                <input type="hidden" id="quiz_id" name="quiz_id" value="{{ $quiz_view_id }}">
                {!! Form::close() !!}

                {{--<h1 id="message" style="display: none">Brush up a litle bit and come back again</h1>--}}
                {{--<div id="back_button" style="display: none" class="col-md-12 top-margin-50"><button class="btn btn-lg btn-success"  value="">Back</button></div>--}}

            </div>
        </div>


    </div>




@endsection
