@extends('layouts.header')
@section('title', 'View Course Result')

@section('content')
    <script src="/js/quiz.js"></script>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 top-margin-50">
                <div class="lms_default_tab">

                    <div id="myTabContent" class="tab-content">

                        <div class="tab-pane text-center active" id="myAnalytics">

                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h1><a href="/course/tutorial/{{ $syllabus->id }}">{{ $syllabus->unit_title }}</a></h1>
                                <hr>
                                <canvas id="myChart" style="display: block;"></canvas>
                            </div>


                            <?php

                            $backgroundColor[0] = "'rgba(255, 99, 132, 0.2)',";
                            $backgroundColor[1] = "'rgba(54, 162, 235, 0.2)',";
                            $backgroundColor[2] = "'rgba(255, 206, 86, 0.2)',";
                            $backgroundColor[3] = "'rgba(75, 192, 192, 0.2)',";
                            $backgroundColor[4] = "'rgba(153, 102, 255, 0.2)',";
                            $backgroundColor[5] = "'rgba(255, 159, 64, 0.2)',";


                            $borderColor[0] = "'rgba(255,99,132,1)',";
                            $borderColor[1] = "'rgba(54, 162, 235, 1)',";
                            $borderColor[2] = "'rgba(255, 206, 86, 1)',";
                            $borderColor[3] = "'rgba(75, 192, 192, 1)',";
                            $borderColor[4] = "'rgba(153, 102, 255, 1)',";
                            $borderColor[5] = "'rgba(255, 159, 64, 1)',";

                            ?>


                            <script>

                                ctx = document.getElementById("myChart").getContext('2d');

                                var myChart = new Chart(ctx, {
                                    type: 'bar',
                                    data: {
                                        labels: [<?php
                                            for ($i=0; $i<count($analytics); $i++) {
                                                echo '"Quiz '. ( $i+1) .' ('. $analytics[$i] . '%)' . '" , ';
                                            }
                                            ?>],
                                        datasets: [{
                                            label: '%',
                                            data: [<?php
                                                for ($i=0; $i<count($analytics); $i++) {
                                                    echo $analytics[$i] . ' , ';
                                                }
                                                ?>],
                                            backgroundColor: [
                                                <?php
                                                for ($i=0; $i<count($analytics); $i++) {
                                                    echo $backgroundColor[$i%6];
                                                }
                                                ?>
                                            ],
                                            borderColor: [
                                                <?php
                                                for ($i=0; $i<count($analytics); $i++) {
                                                    echo $borderColor[$i%6];
                                                }
                                                ?>
                                            ],
                                            borderWidth: 1
                                        }]
                                    },
                                    options: {
                                        legend: { display: false },

                                        scales: {
                                            xAxes: [{
                                                barPercentage: 0.30,
                                                ticks: {
                                                    fontSize: 30
                                                }
                                            }],
                                            yAxes: [{
                                                ticks: {
                                                    fontSize: 30,
                                                    beginAtZero:true,
                                                    min: 0,
                                                    max: 100,
                                                    callback: function(value) {
                                                        return value + "%"
                                                    }
                                                },
                                            }]
                                        }
                                    }
                                });
                            </script>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
