@extends('layouts.header')
@section('title', 'Schedule')

@section('content')


    <div class="container container-height">
        <div class="lms_course_syllabus lms_offer_courses">
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-sm-12 top-margin-50">
                    <div class="card ">
                        <div class="card-body">

                            <div class="row mb-2">
                                <div class="col-sm-4">
                                    <button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#add_schedule_modal">
                                        Add New Schedule
                                    </button>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-centered table-striped" id="products-datatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>No of Students</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($schedules as $schedule)
                                        <tr>
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{ formatDate($schedule->schedule_date) }}</td>
                                            <td>{{ date('g:i a', strtotime( $schedule->start_time )) }}</td>
                                            <td>{{ date('g:i a', strtotime( $schedule->end_time )) }}</td>
                                            <td>{{ $schedule->no_of_guest }}</td>
                                            <td>
                                                <a type="button" href="/schedule/{{ $schedule->schedule_id }}" class="btn btn-primary waves-effect waves-light">View</a>

                                                <button type="button" class="btn btn-danger waves-effect waves-light">
                                                    <span class="btn-label"><i class="mdi mdi-close-circle-outline"></i></span>Delete Schedule
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach



                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="add_schedule_modal">
        <div class="modal-dialog" style="margin-top: 150px">
            <div class="modal-content">
                <form role="form" action="/add/schedule" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Add New Schedule</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="course_name">Schedule Date</label>
                            <input type="text" class="form-control" id="schedule_date" name="schedule_date" data-provide="datepicker" data-date-autoclose="true" value="{{ date('m/d/Y') }}">
                        </div>

                        <div class="form-group">
                            <label for="start_time">Start Time</label>
                            <input type="text" id="preloading-timepicker" name="start_time" class="form-control flatpickr-input active" placeholder="Start Time" readonly="readonly">

                        </div>

                        <div class="form-group">
                            <label for="end_time">End Time</label>
                            <input type="text" id="basic-timepicker" name="end_time" class="form-control flatpickr-input active" placeholder="End Time" readonly="readonly">

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <input type="submit" class="btn btn-default" value="Create">
                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection