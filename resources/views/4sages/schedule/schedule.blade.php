@extends('layouts.header')
@section('title', 'Schedule')

@section('content')


    <div class="container container-height">
        <div class="lms_course_syllabus lms_offer_courses">
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-sm-12 top-margin-50">
                    <div class="card ">
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-centered table-striped" id="products-datatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Registered On</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($appointments as $appointment)
                                        <tr>
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{ $appointment->user->name }}</td>
                                            <td>{{ $appointment->user->email }}</td>
                                            <td>{{ formatDateTime($appointment->created_at) }}</td>
                                            <td>
                                                <button type="button" class="btn btn-danger waves-effect waves-light">
                                                    <span class="btn-label"><i class="mdi mdi-close-circle-outline"></i></span>Cancel Appointment
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach



                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection