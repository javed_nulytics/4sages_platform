@extends('layouts.header')
@section('title', 'Profile')

@section('content')

    <script src="/js/user.js"></script>

    @foreach($tutorial_completed as $item)
        <script type="application/javascript">
            <?php echo 'pieChart_array_push("'. getSyllabusName($item->syllabus_id).'", "'.$item->completed_percentage.'");'; ?>
        </script>
    @endforeach

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="lms_title_center">
                    <div class="lms_heading_1">
                        <h2 class="lms_heading_title">Dashboard</h2>
                    </div>
                    <p id="message_display"></p>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="lms_experts_team_image" id="lms_experts_team_image11">
                    <img class="img-responsive" src="/profile_photo/preview/{{$user_data->image_title}}" style="border-radius: 10px" />


                    <form action="/upload/profile_photo" id="profile_photo_form" method="post"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <span class="btn btn-default btn-file center-block">
                            <input type="hidden" name="user_id" value="{{$user_data->id}}">
                            Upload Picture <input type="file" name="image" id="upload_photo_button" accept="image/*">
                        </span>
                    </form>

                </div>
            </div>


            <div class="col-lg-9">
                <h3>
                    @if($user_data->first_name != "")
                        {{$user_data->first_name}} {{$user_data->last_name}}
                    @else {{$user_data->name}}
                    @endif
                </h3>
                <hr>
                <div class="lms_default_tab">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#info" role="tab" data-toggle="tab">Info</a></li>
                        <li><a href="#scoreTable" role="tab" data-toggle="tab">Quiz Scores</a></li>
                        <li><a href="#myClass" role="tab" data-toggle="tab">My Classes</a></li>
                        <li><a href="#myAnalytics" role="tab" data-toggle="tab">Analytics</a></li>
                        @if (Auth::user()->role == "Admin")
                            <li><a href="#userControl" role="tab" data-toggle="tab">Users</a></li>
                            <li><a href="#studentRank" role="tab" data-toggle="tab">Ranking</a></li>
                        @endif

                    </ul>
                    <div style ="font-size: 20px" id="myTabContent" class="tab-content">

                        <div class="tab-pane active" id="info">
                            <h3 id="name_info">
                                @if($user_data->first_name != "")
                                    {{$user_data->first_name}} {{$user_data->last_name}}
                                @else {{$user_data->name}}
                                @endif
                            </h3>
                            <p id="bio_info">{{$user_data->bio}}</p>

                            <h4 class="top-margin-30">Update Your info</h4>

                            <div class="col-lg-5 col-md-5 col-sm-5 lms_login_body">
                                <div role="form">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input id="name" placeholder="Display Name" type="text" class="form-control" value="{{$user_data->name}}">
                                    </div>

                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input id="first_name" placeholder="First Name" type="text" class="form-control" value="{{$user_data->first_name}}">
                                    </div>

                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input id="last_name" placeholder="Second Name" type="text" class="form-control" value="{{$user_data->last_name}}">
                                    </div>

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" value="{{$user_data->email}}" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label>Bio</label>
                                        <input type="text" id="bio" class="form-control" value="{{$user_data->bio}}" >
                                    </div>


                                    <button class="btn btn-default" id="update_user_button">Update</button>
                                </div>
                            </div>


                        </div>

                        <div style ="font-size: 20px" class="tab-pane" id="scoreTable">
                            {{--@if(checkStudentRanking(Auth::user()->id, 20))--}}
                            {{--<h3 id="topRankStudent">You are in top 20%</h3>--}}
                            {{--@endif--}}

                            {{--<h3 id="topRankStudent">You are ahead of {{ ceil(calculatePercentile_1(Auth::user()->id)) }}% scholars</h3>--}}

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course Topic</th>
                                    <th>Score</th>
                                    <th>Percentile Rank</th>
                                </tr>

                                </thead>
                                <tbody>

                                @php $quiz_count = 1; @endphp

                                @foreach($quiz_data as $quiz)
                                    <tr>
                                        <th scope="row"> {{ $quiz_count++ }}</th>
                                        <td>{{$quiz->unit_title}}</td>
                                        <td>{{$quiz->score}}%</td>
                                        <td>You are ahead of {{$quiz->percentile}}% scholars who took this quiz</td>
                                    </tr>
                                @endforeach



                                </tbody>
                            </table>

                        </div>

                        <div class="tab-pane" id="myClass">

                            <ul class="lms_underlist_doted">
                                @foreach($course_data as $course)
                                    <li><a href='/course/{{$course->course_id}}'>{{$course->course_name}}</a></li>
                                @endforeach
                            </ul>

                        </div>


                        <div class="tab-pane text-center" id="myAnalytics">

                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <h3>Progress</h3>
                                <hr>
                                <canvas id="myChart"></canvas>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <h3>Tutorial Completed</h3>
                                <hr>
                                <canvas id="myPieChart" ></canvas>
                            </div>



                            <?php

                            $backgroundColor[0] = "'rgba(255, 99, 132, 0.2)',";
                            $backgroundColor[1] = "'rgba(54, 162, 235, 0.2)',";
                            $backgroundColor[2] = "'rgba(255, 206, 86, 0.2)',";
                            $backgroundColor[3] = "'rgba(75, 192, 192, 0.2)',";
                            $backgroundColor[4] = "'rgba(153, 102, 255, 0.2)',";
                            $backgroundColor[5] = "'rgba(255, 159, 64, 0.2)',";


                            $borderColor[0] = "'rgba(255,99,132,1)',";
                            $borderColor[1] = "'rgba(54, 162, 235, 1)',";
                            $borderColor[2] = "'rgba(255, 206, 86, 1)',";
                            $borderColor[3] = "'rgba(75, 192, 192, 1)',";
                            $borderColor[4] = "'rgba(153, 102, 255, 1)',";
                            $borderColor[5] = "'rgba(255, 159, 64, 1)',";

                            ?>
                            <script>




                                ctx = document.getElementById("myChart").getContext('2d');

                                var myChart = new Chart(ctx, {
                                    type: 'bar',
                                    data: {
                                        labels: [<?php
                                            for ($i=0; $i<count($analytics); $i++) {
                                                echo '"' .$analytics[$i][0] . ' ('. $analytics[$i][1] . '%)' . '" , ';
                                            }
                                            ?>],
                                        datasets: [{
                                            label: '%',
                                            data: [<?php
                                                for ($i=0; $i<count($analytics); $i++) {
                                                    echo $analytics[$i][1] . ' , ';
                                                }
                                                ?>],
                                            backgroundColor: [
                                                <?php
                                                for ($i=0; $i<count($analytics); $i++) {
                                                    echo $backgroundColor[$i%6];
                                                }
                                                ?>

                                            ],
                                            borderColor: [
                                                <?php
                                                for ($i=0; $i<count($analytics); $i++) {
                                                    echo $borderColor[$i%6];
                                                }
                                                ?>
                                            ],
                                            borderWidth: 1
                                        }]
                                    },
                                    options: {
                                        legend: { display: false },

                                        scales: {
                                            xAxes: [{
                                                barPercentage: 0.15
                                            }],
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero:true,
                                                    min: 0,
                                                    max: 100,
                                                    callback: function(value) {
                                                        return value + "%"
                                                    }
                                                }
                                            }]
                                        }
                                    }
                                });
                            </script>

                        </div>

                        @if (Auth::user()->role == "Admin")
                            <div class="tab-pane" id="userControl">

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Active</th>
                                        <th>Temporary ID</th>
                                        <th>Joined</th>
                                    </tr>

                                    </thead>
                                    <tbody>

                                    @php $user_count = 1; @endphp

                                    @foreach($users as $user)
                                        <tr>
                                            <th scope="row"> {{ $user_count++ }}</th>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>

                                            <td>
                                                <select class="form-control user-role">
                                                    <option value="Admin-{{$user->id}}" {{$user->role=="Admin" ? "selected" : ""}}>Admin</option>
                                                    <option value="Student-{{$user->id}}" {{$user->role=="Student" ? "selected" : ""}}>Student</option>
                                                </select>
                                            </td>

                                            <td>
                                                <div class="onoffswitch">
                                                    <input type="checkbox" class="onoffswitch-checkbox" id="myonoffactive-{{$user->id}}" {{$user->active ? "checked" : ""}} value="active-{{$user->id}}">
                                                    <label class="onoffswitch-label" for="myonoffactive-{{$user->id}}">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="onoffswitch">
                                                    <input type="checkbox" class="onoffswitch-checkbox" id="myonofftemp-{{$user->id}}" {{$user->temporary_id ? "checked" : ""}} value="temporary_id-{{$user->id}}">
                                                    <label class="onoffswitch-label" for="myonofftemp-{{$user->id}}">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>{{ date('F j, Y', strtotime( $user->created_at )) }}</td>
                                        </tr>
                                    @endforeach



                                    </tbody>
                                </table>

                            </div>
                        @endif


                        @if (Auth::user()->role == "Admin")



                            <div class="tab-pane" id="studentRank">

                                <a class="btn btn-default text-center" href="/report/download">Download Report</a>

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Ranking</th>
                                        <th>Name</th>
                                        <th>Percentage</th>
                                        <th>Percentile</th>
                                    </tr>

                                    </thead>
                                    <tbody>

                                    @php $count = 1; @endphp

                                    @foreach($ranks as $rank)
                                        <tr>
                                            <th scope="row">{{$count++}}</th>
                                            <td><a href="/view/report/{{ $rank->id }}">{{$rank->name}}</a></td>
                                            <td>{{$rank->percentage}}%</td>
                                            <td>{{$rank->percentile}}%</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        @endif


                    </div>
                </div>

            </div>



        </div>
    </div>



@endsection
