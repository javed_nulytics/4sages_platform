<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title></title>

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>

<div class="container">

  <h1 class="text-center mt-5">4SAGES</h1>
  <h4 class="text-center">Report Generated : {{ date('F j, Y', strtotime( date('Y-m-d H:i:s') )) }}</h4>

  <table class="table table-striped" style="margin-top: 50px">
    <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Percentage (%)</th>
      <th scope="col">Percentile</th>
    </tr>
    </thead>
    <tbody>

    @php $count = 1; @endphp

    @foreach($ranks as $rank)
      <tr>
        <th scope="row">{{$count++}}</th>
        <td>{{$rank->name}}</td>
        <td>{{$rank->percentage}}%</td>
        <td>{{$rank->percentile}}%</td>
      </tr>
    @endforeach

    </tbody>
  </table>
</div>

</body>
</html>







