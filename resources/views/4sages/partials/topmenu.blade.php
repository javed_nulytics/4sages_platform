<div class="navbar-collapse nav-main-collapse collapse">
  <div class="container">

    <!-- edit course Modal -->
    <div class="modal fade" id="add_course_modal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="margin-top: 200px;">
          <form role="form" action="/addcourse" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">x</button>
              <h4 class="modal-title">Add Curriculum</h4>
            </div>
            <div class="modal-body">
              <!-- all the content here-->

              <div class="form-group">
                <label for="course_name">Curriculum Name</label>
                <input type="text" class="form-control" id="course_name" name="course_name" placeholder="Curriculum Name">
              </div>

              <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" id="description" name="description" placeholder="Description">
              </div>

              <div class="form-group">
                <label for="course_overview">Curriculum Overview</label>
                <textarea class="form-control" id="course_overview" name="overview"
                          placeholder="Curriculum Overview"></textarea>
              </div>

              <div class="form-group">
                <label for="overview">Image</label>
                <input type="file" class="form-control" id="image" name="image" placeholder="Course Image">
              </div>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <input type="submit" id="course_update" class="btn btn-default" value="Create">
            </div>
          </form>
        </div>

      </div>
    </div>
    <!-- end Modal -->


    <nav class="nav-main mega-menu">
      <ul class="nav nav-pills nav-main" id="mainMenu">


        <li class="dropdown">
          <a class="dropdown-toggle" href="https://www.4sages.com">Home</a>
        </li>




        @if (Auth::check())
          <li class="dropdown"> <a class="dropdown-toggle" href="/courselist">Curriculum<i class="icon icon-angle-down"></i> </a></li>
          <li class="dropdown">
            <a class="dropdown-toggle" href="/profile">Profile
              <i class="icon icon-angle-down"></i>
            </a>
          </li>

          <li class="dropdown"><a class="dropdown-toggle" href="/course/lifescience">Life Science</a></li>
          <li class="dropdown"><a class="dropdown-toggle" href="/course/physicalscience">Physical Science</a></li>
          <li class="dropdown"><a class="dropdown-toggle" href="/course/earthscience">Earth Science</a></li>
        @endif



        {{--<li class="dropdown"><a class="dropdown-toggle">About <i class="icon icon-angle-down"></i> </a>--}}
        {{--<ul class="dropdown-menu">--}}
        {{--<li><a href="/aboutus">What we do</a></li>--}}
        {{--<li><a href="/team">Team</a></li>--}}
        {{--<li class="dropdown"><a class="dropdown-toggle">Blog</a></li>--}}
        {{--</ul>--}}
        {{--</li>--}}




        @if (!Auth::check())
          <li class="dropdown"> <a class="dropdown-toggle" href="/login"> Login <i class="icon icon-angle-down"></i> </a></li>
          {{--<li class="dropdown"> <a class="dropdown-toggle" href="/register"> Register <i class="icon icon-angle-down"></i> </a></li>--}}
        @else
          <li class="dropdown"> <a class="dropdown-toggle" href="/logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> Logout <i class="icon
            icon-angle-down"></i> </a></li>

          <form id="logout-form" action="/logout" method="POST" style="display: none;">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
        @endif
      </ul>
    </nav>
    {{--<div class="lms_search_toggle"><a><i class="fa fa-search"></i></a></div>--}}
    {{--<div class="lms_search_wrapper"><input type="search" placeholder="Search..." /></div>--}}
  </div>
</div>