@extends('layouts.header')
@section('title', 'Quiz')

@section('content')
    <script src="/js/quiz.js"></script>
    <link rel="stylesheet" href="/css/radio-button.css"/>
    <link href="/css/magic-check.css" rel="stylesheet">

    <div class="container">
        <div class="row">
            <div class="col-lg-12" id="myform">

                <input type="hidden" id="quiz_view_id" value="{{$quiz_view_id}}">
                <input type="hidden" id="syllabus_id" value="{{$syllabus_id->course_topic_id}}">
                <input type="hidden" id="token" value="{{csrf_token()}}">


                @php $question_count = 0 @endphp

                @foreach($quiz_data as $quiz)

                    <script type="application/javascript">
                        <?php echo 'store_ans("'.$quiz->answer.'");'; ?>
                    </script>

                    <div class="col-md-12 top-margin-50">
                        <h4>{{++$question_count}}. {{$quiz->question}}</h4>

                        <div class="col-md-4">
                            @if($quiz->media != "")
                                <img src="/questionimage/{{$quiz->media}}" width="40%" style="border-radius: 5px">
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-8">

                                <div class="">
                                    <input type="radio" class='magic-radio' name="option[{{$quiz->quiz_id}}]" id="option_id_{{$quiz->quiz_id}}_1" value="{{$quiz->option_1}}"checked/>
                                    <label for="option_id_{{$quiz->quiz_id}}_1">{{$quiz->option_1}}</label>
                                </div>

                                <div class="">
                                    <input type="radio" class='magic-radio' name="option[{{$quiz->quiz_id}}]" id="option_id_{{$quiz->quiz_id}}_2" value="{{$quiz->option_2}}"/>
                                    <label for="option_id_{{$quiz->quiz_id}}_2">{{$quiz->option_2}}</label>
                                </div>

                                <div class="">
                                    <input type="radio" class='magic-radio' name="option[{{$quiz->quiz_id}}]" id="option_id_{{$quiz->quiz_id}}_3" value="{{$quiz->option_3}}"/>
                                    <label for="option_id_{{$quiz->quiz_id}}_3">{{$quiz->option_3}}</label>
                                </div>

                            </div>
                        </div>

                    </div>





                @endforeach


                <div class="col-md-12 top-margin-50">
                    <button type="submit" class="btn btn-lg btn-success" id="submit_quiz_button" value="Submit Quiz">Submit</button>
                </div>

                <div class="col-md-12">
                    <a type="button" class="btn btn-lg btn-success" id="back_button" href="#" style="display: none">Back To Topic</a>
                </div>

                <div class="col-md-12">
                    <button type="button" class="btn btn-lg btn-success" id="another_quiz"  style="display: none">Wanna take another quiz?</button>
                </div>

                {!! Form::open(['url' => '/course/quiz/', 'id' => 'quiz_form']) !!}
                <input id="course_quiz" name="course_quiz" type="hidden" >
                {!! Form::close() !!}

                {{--<h1 id="message" style="display: none">Brush up a litle bit and come back again</h1>--}}
                {{--<div id="back_button" style="display: none" class="col-md-12 top-margin-50"><button class="btn btn-lg btn-success"  value="">Back</button></div>--}}

            </div>
        </div>


    </div>




@endsection
