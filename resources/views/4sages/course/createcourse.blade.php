<div class="col-lg-12 col-md-6">
    <div class="lms_login_window lms_login_light">
        <h3>Create Course</h3>
        <div class="lms_login_body">
            <form role="form" action="/addcourse" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}
            <div class="form-group">
                <label for="course_name">Course Name</label>
                <input type="text" class="form-control" id="course_name" name="course_name" placeholder="Course Name" required>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" id="description" name="description"
                       placeholder="Description">
            </div>

            <div class="form-group">
                <label for="overview">Course Overview</label>
                <input type="text" class="form-control" id="overview" name="overview" placeholder="Course Overview">
            </div>

            <div class="form-group">
                <label for="overview">Image</label>
                <input type="file" class="form-control" id="image" name="image" placeholder="Course Image" required>
            </div>

            <button type="submit" class="btn btn-default" >Create Course</button>

            </form>
        </div>
    </div>
</div>