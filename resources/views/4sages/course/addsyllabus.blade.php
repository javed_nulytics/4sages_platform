<div class="col-lg-12 col-md-6">
    <div class="lms_login_window lms_login_light">
        <h3>Create Course</h3>
        <div class="lms_login_body">
            <form role="form" action="/addsyllabus" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}
            <input type="hidden" name="course_id" id="course_id" value="{{$course_id}}">
            <div class="form-group">
                <label for="week_no">Week No.</label>
                <input type="text" class="form-control" id="week_no" name="week_no" placeholder="Week no." required>
            </div>
            <div class="form-group">
                <label for="unit_title">Unit Title</label>
                <input type="text" class="form-control" id="unit_title" name="unit_title"
                       placeholder="Unit Title">
            </div>

            <div class="form-group">
                <label for="unit_overview">Unit Overview</label>
                <input type="text" class="form-control" id="unit_overview" name="unit_overview" placeholder="Unit Overview">
            </div>

            <div class="form-group">
                <label for="unit_image">Image</label>
                <input type="file" class="form-control" id="unit_image" name="unit_image" placeholder="Course Image/Video" >
            </div>

            <button type="submit" class="btn btn-default" >Create Unit Syllabus</button>

            </form>
        </div>
    </div>
</div>