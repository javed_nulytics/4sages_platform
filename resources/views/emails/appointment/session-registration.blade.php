@component('mail::message')
# Hi {{ $data['name'] }}

You have successfully registered to the 4sages session which will going to held on {{ formatDate($data['schedule_date']) }} - from {{ $data['start_time'] }} to {{ $data['end_time'] }}.

Thanks,<br>
Team 4sages
@endcomponent
