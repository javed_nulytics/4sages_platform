$(document).ready(function() {

    $('.tutorial_logo').addClass('animated slideInLeft');

    $('select').on('change', function() {
//alert( this.value );

//alert("week test : " + week_no);

        var title = $('#title_' + this.value).text();
        var overview = $('#week_' + this.value).text();
        $('#edit_unit_overview').text(overview);
        $('#edit_unit_title').val(title);
        $('#week_no').val(this.value);


    });

    $("#syllabus_update").click(function(){
        //   //new values

        var edit_unit_title = $("#edit_unit_title").val();
        var edit_unit_overview = $("#edit_unit_overview").val();
        var course_id = $("#course_id").val();
        var week_no = $("#week_no").val();
        console.log(unit_title);
        console.log(unit_overview);
        console.log(course_id);

        if(edit_unit_title == "" || edit_unit_overview == "" || week_no == "--"){
            alert ("Invalid input");
        }
        else {
            var params = {
                edit_unit_title: edit_unit_title,
                edit_unit_overview: edit_unit_overview,
                week_no : week_no
            };

            $.ajax({
                url: '/editsyllabus',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},  //send these values to controller

                success: function (response) {
                    console.log(response);
                    $(edit_unit_title).text('#title_' + this.value);
                    $(edit_unit_overview).text('#week_' + this.value);

                    $('#title_' + week_no).text(edit_unit_title);
                    $('#week_' + week_no).text(edit_unit_overview);
                    $('#message_display').text("Successfully edited. ");


                    $("#collapseThree").collapse('hide');



                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
            console.log(params);
        }
    });

    // $("#add_syllabus").click(function() {
    //     var week_no = $("#week_no").val();
    //     var unit_title = $("#unit_title").val();
    //     var unit_overview = $("#unit_overview").val();
    //     var course_id = $("#course_id").val();
    //     if (week_no == "" || unit_title == "" || unit_overview == "") {
    //         alert("Please fill up the gaps first !!!");
    //     }
    //     else {
    //         var params = {
    //             week_no: week_no,
    //             unit_title: unit_title,
    //             unit_overview: unit_overview,
    //             course_id: course_id
    //         };
    //         $.ajax({
    //             url: '/add/syllabus',
    //             type: 'POST',
    //             data: {params: params, "_token": $('#token').val()},  //send these values to controller
    //
    //             success: function (response) {
    //                 console.log(response);
    //
    //                 var htmlstr = '<div class="col-lg-4 col-md-4 col-sm-6" id="syllabus_'+response+'"><div class="lms_video"><div><button id="'+response+'" class="button_delete_syllabus" type="button" >delete</button> </div><div class="lms_hover_section"><img src="/images/courses/course_syllabus1.jpg" alt=""><div class="lms_hover_overlay"><a class="lms_image_link_play"></a></div></div><a href="/course/tutorial/'+response+'"><h3>Week '+week_no+': <div id="title_'+week_no+'">'+unit_title+'</h3></a><p id="week_'+week_no+'">'+unit_overview+'</p></div></div>';
    //
    //                 $('#syllabus_list').append(htmlstr);
    //
    //
    //             },
    //             error: function (error) {
    //                 console.log("Error! Something went wrong.");
    //             }
    //
    //         });
    //
    //     }
    //
    // });



    $(".button_delete_syllabus").click(function() {

        var id = $(this).attr('id');
        //console.log("syllabus id : " + id);

        var url = '/syllabus/delete/' + id;

        $.ajax({
            url: url,
            type: 'POST',
            data: {"_token": $('#token').val()},  //send these values to controller

            success: function (response) {
                console.log(response);

                $( "#syllabus_" + id ).remove();

            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }

        });



    });


    $("#syllabus_delete").click(function(){

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {

            var syllabus_id = $("#syllabus_id").val();

            var params = {
                syllabus_id: syllabus_id,
            };

            $.ajax({
                url: '/syllabus/delete/',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal({
                        type: 'success',
                        html: $('<div>')
                            .addClass('some-class')
                            .text('Syllabus has been deleted'),
                        animation: false,
                        customClass: 'animated bounceIn'
                    });

                    setTimeout(function(){
                        window.location.href = "/course/" + response;
                    },2000);
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        }, function (dismiss) {
            if (dismiss === 'cancel') {
                swal({
                    title: 'Cancelled',
                    type: 'error',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('Your Curriculum is safe :)'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            }
        });


    });


}); <!-- ajax ends here-->











