var start_time = Date.now();
var render_time = 0;
var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
var category_array = [];

window.onload = function(){
    // render_time = parseInt((Date.now()) - start_time);
    //$('#render_time').text(((Date.now()) - start_time)/1000.0 + 's');
};

$(document).ready(function() {

    //currentTime();

    $('#select_en').click(function(){

        var params = {
            locale: 'en'
        };

        $.ajax({
            url: '/change/lang',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function(response) {
                reloadCurrentPage();
            },
            error: function(error) {
                showErrorNotification();
            }

        });
    });


    $('#select_bn').click(function(){

        var params = {
            locale: 'bn'
        };

        $.ajax({
            url: '/change/lang',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function(response) {
                reloadCurrentPage();
            },
            error: function(error) {
                showErrorNotification();
            }

        });
    });


    $('#save_user_settings').click(function(){

        var theme = $('#user_theme').val();

        var params = {
            theme: theme
        };

        $.ajax({
            url: '/change/theme',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function(response) {
                reloadCurrentPage();
            },
            error: function(error) {
                showErrorNotification();
            }

        });
    });


    $("#add_customer_button").click(function(){

        var name = $('#name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var account_no = $('#account_no').val();
        var address = $('#address').val();
        var discount_percentage = $('#discount_percentage').val();
        var accounts_receivable = $('#accounts_receivable').val()!=''?$('#accounts_receivable').val():0;

        if (name != '' && phone != '') {

            var params = {
                name: name,
                email: email,
                phone: phone,
                account_no: account_no,
                address: address,
                discount_percentage: discount_percentage,
                accounts_receivable: accounts_receivable,
            };

            $.ajax({
                url: '/add/customer',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#name').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    $('#account_no').val('');
                    $('#address').val('');
                    $('#discount_percentage').val('0');
                    $('#accounts_receivable').val('0');

                    /*
                     For Customer Page
                     */

                    try {

                        var htmlstr = '<tr id="customer_'+response+'">' +
                            '<td>#</td>' +
                            '<td id="name_'+response+'">'+name+'</td>' +
                            '<td id="address_'+response+'">'+address+'</td>' +
                            '<td id="phone_'+response+'">'+phone+'</td>' +
                            '<td id="account_no_'+response+'">'+account_no+'</td>' +
                            '<td id="discount_percentage_'+response+'">'+discount_percentage+'%</td>' +
                            '<td>&#2547;'+accounts_receivable+'</td>' +
                            '<td><button type="button" class="btn btn-blue waves-effect waves-light" onclick="editCustomer(\''+response+'\', \''+name+'\', \''+email+'\', \''+address+'\', \''+phone+'\', \''+account_no+'\', \''+discount_percentage+'\')">' +
                            '<i class="fe-edit"></i></button> ' +
                            '<button type="button" class="btn btn-danger waves-effect waves-light" onclick="deleteCustomer('+response+')">' +
                            '<i class="fe-trash-2"></i></button></td></tr>';

                        $( "#customer_table" ).append(htmlstr);

                    } catch (e) {

                    }





                    /*
                     For Order/Sale Page
                     */

                    try {

                        var html_str = '<option value="'+response+'">'+name+'-'+phone+'-'+account_no+'</option>';

                        $( "#customer_id" ).append(html_str);
                        $( "#customer_id" ).val(response);

                    } catch(e) {

                    }



                    showSuccessNotification('Customer Has Been Added');

                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

        else showWarningNotification('Input Customer Name and Phone Number');
    });


    $('#clear_cache_button').click(function(){

        $.ajax({
            url: '/clear/cache',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val()},

            success: function(response) {
                showSuccessNotification('Cache Cleared');
                reloadCurrentPage();
            },
            error: function(error) {
                showErrorNotification();
            }

        });
    });


});



function numberPadding(num) {

    var number = num.toString();
    var padding = '';
    var length = 6;

    var number_length = number.length;

    for (var i=0; i<(length-number_length); i++) padding += '0';

    return (padding + number);

}


function formatInvoiceDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return (year  + month + day);
}


function randomString(length) {
    var text = "";
    var d = new Date();
    var time = d.getTime();
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text + '-' + time ;
}


function getCurrentDate() {

    var now = new Date();
    var thisMonth = months[now.getMonth()];
    var date = now.getDate();
    var year = now.getFullYear();

    var today = thisMonth + ' ' + date + ', ' + year;
    return today;
}


function getCurrentTime() {
    var time = new Date();
    return time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
}


function getTimeDifference(in_time, out_time) {

    var startDate = new Date("January 1, 1970 " + in_time);
    var endDate = new Date("January 1, 1970 " + out_time);
    var timeDiff = Math.abs(startDate - endDate);

    var hh = Math.floor(timeDiff / 1000 / 60 / 60);
    if(hh < 10) {
        hh = '0' + hh;
    }
    timeDiff -= hh * 1000 * 60 * 60;
    var mm = Math.floor(timeDiff / 1000 / 60);
    if(mm < 10) {
        mm = '0' + mm;
    }
    timeDiff -= mm * 1000 * 60;
    var ss = Math.floor(timeDiff / 1000);
    if(ss < 10) {
        ss = '0' + ss;
    }

    var output = {
        hour: hh,
        minute: mm,
        second: ss,
    };
    return output;

}


function showProcessingNotification() {

    swal({
        title: "Please Wait!",
        type: 'warning',
        showCancelButton: false,
        showConfirmButton: false
    });

}


function showSuccessNotification(message) {

    swal({
        title: "Done!",
        text: message,
        type: 'success',
        showCancelButton: false,
        showConfirmButton: false,
        timer: 1500
    });

}


function showWarningNotification(message) {

    swal({
        title: "Warning!",
        text: message,
        type: 'warning',
        showCancelButton: false,
        showConfirmButton: false,
        timer: 1500
    });

}


function showErrorNotification(message) {
    message = typeof message !== 'undefined' ? message : 'Something Went Wrong!';

    swal({
        title: "Error!",
        text: message,
        type: 'error',
        showCancelButton: false,
        showConfirmButton: false,
        timer: 1500
    });
}


function reloadCurrentPage() {
    window.location = window.location.pathname;
}


function checkDate(start_date, end_date) {

    var CurrentDate = new Date();
    start_date = new Date(start_date);
    end_date = new Date(end_date);
    var flag = false;

    if(CurrentDate >= start_date && CurrentDate <= end_date) flag = true;
    return flag;
}


function getUserName() {
    return $('#user_name').val();
}


function formatDate(date) {
    var d = new Date(date),
        month = months[d.getMonth()],
        day = '' + d.getDate(),
        year = d.getFullYear();

    //if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return month + ' ' + day + ', ' + year;
}


function formatDateTime(date) {
    var d = new Date(date),
        hour = '' + d.getHours(),
        minute = '' + d.getMinutes(),
        second = '' + d.getSeconds(),
        month = months[d.getMonth()],
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (day.length < 2) day = '0' + day;
    if (hour.length < 2) hour = '0' + hour;
    if (minute.length < 2) minute = '0' + minute;
    if (second.length < 2) second = '0' + second;

    return month + ' ' + day + ', ' + year + ' ' + hour + ':' + minute + ':' + second;
}


function uc_first(string) {
    return string.substr(0,1).toUpperCase()+string.substr(1);
}


function redirect(url, time) {

    setTimeout(function(){
        window.location.href = url;
    }, time);

}


function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}


function getPercentage(value, percentage) {
    return parseFloat(parseFloat(value) * (parseFloat(percentage)/100.00));
}


function currentTime() {
    var date = new Date();

    var this_date = date.getDate();
    var month = months[date.getMonth()];
    var year = date.getFullYear();

    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    hour = updateTime(hour);
    min = updateTime(min);
    sec = updateTime(sec);
    document.getElementById("clock").innerText = this_date + ' ' + month + ' ' + year + ' ' + hour + " : " + min + " : " + sec;
    var t = setTimeout(function(){ currentTime() }, 1000);
}


function updateTime(k) {
    if (k < 10) {
        return "0" + k;
    }
    else {
        return k;
    }
}


function countCharacter(textarea, showSpan) {

    var len = $('#' + textarea).val().length;
    $('#' + showSpan).text(len);

}


function loadBranch(company_id) {

    var params = {
        company_id: company_id
    };

    $.ajax({
        url: '/change/branch',
        type: 'POST',
        format: 'JSON',
        data: {'_token': $('#token').val(), params: params},

        success: function(response) {
            reloadCurrentPage();
        },
        error: function(error) {
            showErrorNotification();
        }

    });




}