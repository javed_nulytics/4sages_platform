$(document).ready(function() {

});


function makeAppointment(schedule_id) {

    swal({
        title: 'Are you sure?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirm Appointment',
        cancelButtonText: 'Cancel',
        confirmButtonClass: 'btn btn-success mr-2',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            schedule_id: schedule_id
        };

        $.ajax({
            url: '/appointment/create',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('Congratulation! You have successfully registered');
                reloadCurrentPage();
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });
}