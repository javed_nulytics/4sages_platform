var pieChart_array = [];
var previous_color = -1;

$(document).ready(function() {



    // var myPieChart = document.getElementById("myPieChart");
    var temp_label = [];
    var temp_data = [];
    var temp_backgroundColor = [];

    for (var i = 0; i < pieChart_array.length; i++) {
        temp_label.push([pieChart_array[i][0]] + ' (' + [pieChart_array[i][1]] + '%)');
        temp_data.push(parseInt([pieChart_array[i][1]]));

        temp_backgroundColor.push(getRandomColor());
    }

    new Chart(document.getElementById("myPieChart"), {

        type: 'horizontalBar',
        data: {
            labels: temp_label,
            datasets: [
                {
                    label: "Population (millions)",
                    backgroundColor: temp_backgroundColor,
                    data: temp_data
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Showing data based on the video duration you completed'
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        max: 100,
                    }
                }],
                yAxes: [{

                }]
            }
        }
    });


    //
    // var myPieChart_data = {
    //
    //     labels: temp_label,
    //
    //     datasets: [
    //         {
    //             // data: temp_data,
    //             data: [2478,5267,734],
    //             // backgroundColor: temp_backgroundColor
    //             backgroundColor: [
    //                 "#FF5A5E",
    //                 "#f1c40f",
    //                 "#46BFBD",
    //                 "#FDB45C",
    //                 "#34495e",
    //                 "#e74c3c",
    //             ]
    //         }]
    // };
    //
    //
    //
    // var pieChart = new Chart(myPieChart, {
    //     type: 'horizontalBar',
    //     data: myPieChart_data,
    // });

    $("#user_update").click(function(){
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var bio = $("#bio").val();
        var dob = $("#dob").val();

        if (first_name == "" || last_name == "") {
            alert("Please insert name");
        }
        else {

            var params = {
                first_name: first_name,
                last_name: last_name,
                bio: bio,
                dob: dob
            };


            $.ajax({
                url: '/edituser',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    console.log(response);
                    $("#first_name_info").text("First Name : " + first_name);
                    $("#last_name_info").text("Last Name : " + last_name);
                    $("#name_info").text(first_name+" " + last_name);
                    $("#dob_info").text("Date of Birth : " + dob);
                    $("#bio_info").text(bio);
                    $("#message_display").text("Info Updated");

                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }


    });


    $('#upload_photo_button').change(function(){
        this.form.submit();
    });


    $('.onoffswitch-checkbox').change(function(){

        var value = 0;
        if ($(this).is(":checked")) value = 1;

        var key = $(this).val().split("-");

        var params = {
            user_id: key[1],
            db_col: key[0],
            value: value,
        };

        $.ajax({
            url: '/edit/user/status',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal("Info Saved", "User status changed", "success");
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });


    $('.user-role').on('change', function() {
        //console.log("role : " +  this.value );
        var str = this.value;
        var key = str.split("-");

        var params = {
            user_id: key[1],
            db_col: 'role',
            value: key[0],
        };

        $.ajax({
            url: '/edit/user/status',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal("Info Saved", "User role changed", "success");
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });
    })


    $('#update_user_button').click(function () {

        var username = $('#name').val();

        if (username != "") {

            var params = {
                name: username,
                first_name: $('#first_name').val(),
                last_name: $('#last_name').val(),
                bio: $('#bio').val(),
            }

            $.ajax({
                url: '/update/user/info',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal("", "Info Updated Successfully", 'success');

                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }


            })
        }

    })


});


function pieChart_array_push(syllabus_id, completed_percentage) {
    var colors = ["#FF5A5E", "#f1c40f", "#46BFBD", "#FDB45C", "#34495e", "#e74c3c"];
    var key = getRandomInt(0, colors.length-1);
    //console.log(key);
    pieChart_array.push([syllabus_id, completed_percentage, colors[key]]);
}


function getRandomColor() {
    var colors = ['#FF5A5E', '#f1c40f', '#46BFBD', '#FDB45C', '#34495e', '#e74c3c'];
    var key = getRandomInt(0, colors.length-1);
    if (key == previous_color) key++;
    previous_color = key;
    //console.log("random color :" + colors[key]);
    return colors[key];
}


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}