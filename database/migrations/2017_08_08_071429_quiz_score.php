<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuizScore extends Migration {

    public function up() {
        Schema::create('quiz_score', function (Blueprint $table) {
            $table->increments('quiz_score_id');
            $table->integer('quiz_id');
            $table->integer('syllabus_id');
            $table->double('score');
            $table->double('percentile')->default(0.0);
            $table->integer('student_id');
            $table->timestamp('created_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('quiz_score');

    }
}
