<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseInvitation extends Migration {

    public function up() {

        Schema::create('course_invitation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutor_id');
            $table->integer('course_id');
            $table->string('invitation_email');
            $table->integer('student_id')->nullable();
            $table->string('status')->default("pending");
            $table->timestamps();
        });

    }


    public function down() {

        Schema::dropIfExists('course_invitation');

    }
}
