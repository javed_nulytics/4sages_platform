<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Quiz extends Migration {

    public function up() {

        Schema::create('quiz', function (Blueprint $table) {
            $table->increments('quiz_id');
            $table->integer('quiz_view_id');
            $table->longText('question');
            $table->longText('option_1');
            $table->longText('option_2');
            $table->longText('option_3');
            $table->longText('answer');
            $table->string('media')->nullable();
            $table->longText('explanation')->nullable();
            $table->string('explanation_media')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Schema::dropIfExists('quiz');
    }
}
