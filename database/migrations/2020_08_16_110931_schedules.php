<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Schedules extends Migration {

    public function up() {

        Schema::create('schedules', function (Blueprint $table) {

            $table->increments('schedule_id')->unsigned();

            $table->date('schedule_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->integer('max_guest')->default(3);
            $table->integer('no_of_guest')->default(0);

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->timestamps();

        });

    }

    public function down() {
        Schema::dropIfExists('schedules');
    }
}
