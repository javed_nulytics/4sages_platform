<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SyllabusContent extends Migration {

    public function up() {

        Schema::create('syllabus_content', function (Blueprint $table) {

            $table->increments('id');
            $table->string('content_title');
            $table->longText('content_description')->nullable();
            $table->string('media')->default("/video/5Mb.mp4");
            $table->integer('author_id');
            $table->integer('syllabus_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });

    }


    public function down() {
        Schema::dropIfExists('syllabus_content');
    }
}
