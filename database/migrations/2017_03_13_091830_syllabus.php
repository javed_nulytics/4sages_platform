<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Syllabus extends Migration {

    public function up() {
        Schema::create('syllabus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->text('unit_title');
            $table->text('unit_overview')->nullable();
            $table->integer('tutor_id');
            $table->string('unit_image')->default('syllabus_default.jpg');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
       Schema::dropIfExists('syllabus');
    }
}
