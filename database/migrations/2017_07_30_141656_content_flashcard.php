<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContentFlashcard extends Migration {

    public function up() {
        Schema::create('content_flashcard', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('syllabus_id');
            $table->string('flashcard_title');
            $table->longText('flashcard_description');
            $table->string('media');
            $table->string('user_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('content_flashcard');
    }
}
