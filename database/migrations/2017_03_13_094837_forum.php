<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Forum extends Migration {

    public function up() {

        Schema::create('forum', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id');
            $table->integer('course_id');
            $table->text('message');

            $table->timestamps();
        });

    }


    public function down() {

        Schema::dropIfExists('forum');

    }
}
