<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Appointments extends Migration {

    public function up() {

        Schema::create('appointments', function (Blueprint $table) {

            $table->increments('appointment_id')->unsigned();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('schedule_id')->unsigned();
            $table->foreign('schedule_id')->references('schedule_id')->on('schedules');

            $table->timestamps();

        });

    }


    public function down() {
        Schema::dropIfExists('appointments');
    }

}
