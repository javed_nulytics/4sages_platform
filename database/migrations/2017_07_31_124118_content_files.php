<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContentFiles extends Migration {

    public function up() {
        Schema::create('content_files', function (Blueprint $table) {
            $table->increments('content_file_id');
            $table->integer('syllabus_id');
            $table->string('file_title');
            $table->string('file_name');
            $table->string('post_mime_type');
            $table->string('user_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('content_files');
    }
}
